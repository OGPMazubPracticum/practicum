/**
 * @author Kaatje De Koninck en Laura Ceustermans
 *
 * @invar The x coordinate of Mazub's bottom left pixel must be valid.
 * 		 | isValidX(getPixelLeftX())
 * @invar The y coordinate of Mazub's bottom left pixel must be valid.
 * 		 | isValidY(getPixelBottomY())
 *
 */
public class Mazub {
	
	/**
	 * Create an instance of Mazub.
	 * 
	 * @param pixelLeftX
	 *            The x-location of Mazub's bottom left pixel.
	 * @param pixelBottomY
	 *            The y-location of Mazub's bottom left pixel.
	 * @param sprites
	 *            The array of sprite images for Mazub.
	 * @effect The given x location is set as pixelLeftX of the alien
	 *          | setPixelLeftX(pixelLeftX)
	 * @effect The given y location is set as pixelBottomY of the alien
	 *          | setPixelBottomY(pixelBottomY)
	 * @post The array of sprite images is set as sprites of the alien    
	 *          | new.getSprites() == sprites
	 * @throw IllegalArgumentException
	 * 		   If the x and y coordinates of the alien's bottom left pixel are out of reach,
	 * 		   the function throws an exception.
	 * 		   |! isValidX(this.getPixelLeftX()) || ! isValidY(this.getPixelBottomY())
	 */
	public Mazub (int pixelLeftX, int pixelBottomY, Sprite[] sprites) throws IllegalArgumentException{
		if (!isValidX(pixelLeftX) || !isValidY(pixelBottomY))
		    throw new IllegalArgumentException();
		this.setPixelLeftX(pixelLeftX);
		this.setPixelBottomY(pixelBottomY);
		this.sprites = sprites;
	}
	
	/**
	 * Return the current size/dimensions of the given alien.
	 * 
	 * @return an array, consisting of 2 integers width and height, which represent
	 * 		   the width and the height respectively of the current sprite of the alien.
	 * @throws IllegalStateException
	 *         if the array of sprites of the alien has the null reference the method throws an exception
	 * 		   |If this.getCurrentSprite() == null
	 */
	public int[] getSize(){
		if (this.getCurrentSprite() == null)
				throw new NullPointerException();
		int width = this.getCurrentSprite().getWidth();
		int height = this.getCurrentSprite().getHeight();
		int[] size = {width,height};
		return size;
	}

	/**
	 * Return the current location of the given alien.
	 * 
	 * @return an array, consisting of 2 integers {x, y}, that represents the
	 *         coordinates of the given alien's bottom left pixel in the world.
	 * @throws IllegalStateException
	 * 		   If the x and y coordinates of the alien's bottom left pixel are out of reach,
	 * 		   the function throws an exception.
	 * 		   |! isValidX(this.getPixelLeftX()) || ! isValidY(this.getPixelBottomY())
	 */
	public int[] getLocation(){
		if (! isValidX(this.getPixelLeftX()) || ! isValidY(this.getPixelBottomY()) )
			throw new IllegalStateException();
		int result[] = {this.getPixelLeftX(),this.getPixelBottomY()};
		return result;
	}
	
    /**
	 * Set the location of the given alien to the new coordinates.
	 * 
	 * @param pixelLeftX
	 * 		  The new x coordinate of the bottom left pixel of the alien.
	 * @param pixelBottomY
	 * 		  The new y coordinate of the bottom left pixel of the alien.
	 * @effect The x coordinate is changed to its new value.
	 * 		  | setPixelLeftX(pixelLeftX)
	 * @effect The y coordinate is changed to its new value.
	 * 		  | setPixelBottomY(pixelBottomY)
	 */
	public void setLocation(int pixelLeftX, int pixelBottomY) throws IllegalStateException {
		this.setPixelLeftX(pixelLeftX);
		this.setPixelBottomY(pixelBottomY);
	}
	
	/**
	 * Check whether the given integer is in reach of the width of the game world.
	 * 
	 * @param 	pixelLeftX
	 * 		  	The pixel/integer that needs to be checked.
	 * @return 	Returns true if the integer lies between 0 and 1024, with 0 included.
	 * 			| result == (0 <= pixelLeftX && pixelLeftX < 1024)
	 */
	public static boolean isValidX(int pixelLeftX){
		return 0 <= pixelLeftX && pixelLeftX < 1024;
	}
	
	/**
	 * Check whether the given integer is in reach of the height of the game world.
	 * 
	 * @param  	pixelBottomY
	 * 			The integer that needs to be checked.
	 * @return	Returns true if the integer lies between 0 and 768, with 0 included.
	 * 			| result == (0 <= pixelBottomY && pixelBottomY < 768)
	 */
	public static boolean isValidY(int pixelBottomY){
		return 0 <= pixelBottomY && pixelBottomY < 768; 
	}
	
	/**
	 * Return the x coordinate of the bottom left pixel of the given alien.
	 */
	@Basic
	public int getPixelLeftX(){
		return this.pixelLeftX;
	}

	private int pixelLeftX;
	
	/**
	 * Set the x coordinate of the bottom left pixel of the given alien to the given integer.
	 * 
	 * @param 	pixelLeftX
	 * 			The new x coordinate of the given alien.
	 * @post	If the given integer is a valid x coordinate, the x coordinate is set to this new value.
	 * 			| if isValidX(pixelLeftX)
	 * 			| 	(new this).getPixelLeftX() == pixelLeftX
	 * @post	If the given integer is not a valid x coordinate, and the integer is less than zero,
	 * 			the x coordinate is set to 0.
	 * 			| if ( (! isValidX(pixelLeftX)) && ( pixelLeftX < 0)
	 * 			| 	(new this).getPixelLeftX() == 0
	 * @post	If the given integer is not a valid x coordinate, and the integer is bigger than the
	 * 			maximum allowed x coordinate in the game world, the x coordinate is set to 1023, the maximum legal value.
	 * 			| if ( (! isValidX(pixelLeftX)) && ( pixelLeftX > 0)
	 * 			| 	(new this).getPixelLeftX() == 1023
	 */
	public void setPixelLeftX(int pixelLeftX)
	{
		if (! isValidX(pixelLeftX))
			if (pixelLeftX < 0)
				pixelLeftX = 0;
			else
				pixelLeftX = 1023;
		this.pixelLeftX = pixelLeftX;
	}
	
	/**
	 * Return the y coordinate of the bottom left pixel of the given alien.
	 */
	@Basic
	public int getPixelBottomY(){		
		return this.pixelBottomY;
	}
	
/**
	 * Set the y coordinate of the bottom left pixel of the given alien to the given value.
	 * 
	 * @param 	pixelBottomY
	 * 			the new y coordinate
	 * @post  	If the given integer is a valid y coordinate, the y coordinate is set to this new value.
	 *			| if (isValidY(pixelBottomY))
	 * 			|	(new this).getPixelBottomY() == pixelBottomY
	 * @post	If the given integer is not a valid y coordinate, and the integer is less than zero,
	 * 			the y coordinate is set to 0.
	 * 			| if ( (! isValidY(pixelBottomY)) && ( pixelBottomY < 0)
	 * 			| 	(new this).getPixelBottomY() == 0
	 * @post	If the given integer is not a valid y coordinate, and the integer is bigger than the
	 * 			maximum allowed y coordinate in the game world, the y coordinate is set to 767, the maximum legal value.
	 * 			| if ( (! isValidY(pixelBottomY)) && ( pixelBottomY > 0)
	 *			| 	(new this).getPixelBottomY() == 767
	 */
	public void setPixelBottomY(int pixelBottomY)
	{
		if (! isValidY(pixelBottomY))
			if (pixelBottomY < 0)
				pixelBottomY = 0;
			else
				pixelBottomY = 767;
		this.pixelBottomY = pixelBottomY;
	}
	
	private int pixelBottomY;
	
//	Instance variables for the alien's actions.

	// Velocity and acceleration of the alien //
	
	private double[] velocity = {0.0,0.0};
	
	/**
	 * Return the current velocity (in m/s) of the given alien.
	 */
	@Basic
	public double[] getVelocity(){
		return this.velocity;
	}

	/**
	 * Set the velocity of the alien to the new given values.
	 * 
	 * @param velocity
	 *        The new velocity of the alien, x and y components.
	 * @pre	  The given velocity in the x direction must be positive but smaller than the maximum allowed velocity
	 *        in the x direction.
	 *        | velocity[0] >= 0 && velocity[0] <= getMaximumVelocity()
	 * @post  The new velocity of the alien will be changed to the given values.
	 * 		  | new.getVelocity() == velocity;
	 */
	public void setVelocity(double[] velocity)
	{
		this.velocity = velocity;
	}
	
	/**
	 * Return the alien's current acceleration.
	 */
	@Basic
	public double[] getAcceleration()
	{
		return this.acceleration;
	}
	
	/**
	 * Set the acceleration of the alien to the new given values.
	 * @param acceleration
	 * 			The new acceleration of the alien, x and y components.
	 * @post If the given values are either zero or equal to the given possible acceleration
	 * 			values, the new acceleration of the alien will be equal to the given acceleration.
	 * 		| if ( (acceleration[0] == 0.0 || acceleration[0] == getAccelerationValues()[0])
	 *      |	&& (acceleration[1] == 0.0 || acceleration[1]== getAccelerationValues()[1] ))
	 *      | 	new.getAcceleration() == acceleration;
	 */
	public void setAcceleration(double[] acceleration){
		if ( (acceleration[0] == 0.0 || acceleration[0] == getAccelerationValues()[0])
				&& (acceleration[1] == 0.0 || acceleration[1]== getAccelerationValues()[1] ))
			this.acceleration = acceleration;
	}
	
	private double[] acceleration = {0.0,0.0};
	
	// RUNNING //
	
	/**
	 *	Return the current running status of the alien.
	 */
	@Basic
	public Running getRunning(){
		return this.running;
	}
	
	/**
     * Set the running status of the alien to the given status.
	 * 
	 * @param 	running
	 * 		  	The new running status for the alien.
	 * @post	If running is a possible status for the alien, the new running status
	 * 			will be changed to running.
	 * 			| if isValidRunning(running)
	 * 			|	new.getRunning() == running; 
	 */
	public void setRunning(Running running){
		assert isValidRunning(running);
		this.running = running;
	}
	
	/**
	 * Check if the given value is a possible running status for the alien.
	 * 
	 * @param 	running
	 * 		  	The value that needs to be checked.
	 * @return 	If the given value is either Running.Left, Running.Right or Running.STOP,
	 * 			the method will return true.
	 * 			| result == (running == Running.LEFT) || (running == Running.RIGHT) || (running == Running.STOP)
	 */
	public static boolean isValidRunning(Running running){
		return (running == Running.LEFT) || (running == Running.RIGHT) || (running == Running.STOP);
	}
	
	private Running running=Running.STOP;
	
	/**
	 * Make the alien move horizontally.
	 * 
	 * @param   direction
	 *          The direction the alien has to start running in.
	 * @pre		The alien was standing still and not already moving to the left or to the right.
	 * 			| alien.getRunning()==Running.STOP
	 * @effect	The alien's running status will be changed to running in the given direction.
	 * 			| this.setRunning(direction)
	 * @post	The alien's acceleration in x direction is changed to the static acceleration in x direction.
	 * 			The y acceleration of the alien is left unchanged.
	 * 			| new.getAcceleration() == {Mazub.getAccelerationValues()[0],this.getAcceleration()[1]}
	 * @post	The alien's velocity in x direction is changed to the initialVelocity in x direction.
	 * 			The y velocity of the alien is set to zero.
	 * 			| new.getVelocity() == {this.getInitialVelocityX(),0.0}
	 * @post	The last direction in which the alien was running is changed to direction.
	 * 			| setLastDirection(direction)
	 * @post	The time since the alien's last action is reset.
	 * 			| setTimeLastAction(0.0)
	 */
	public void startMove(Running direction){
		assert this.getRunning() == Running.STOP;
		this.setRunning(direction);
		double[] newValuesAcc = {Mazub.getAccelerationValues()[0],this.getAcceleration()[1]};
		this.setAcceleration(newValuesAcc);
		double[] newValuesVel = {this.getInitialVelocityX(),0.0};
		this.setVelocity(newValuesVel);
		setLastDirection(direction);
		setTimeLastAction(0.0);
	}
	
	/**
	 * Make the alien stop moving.
	 * 
	 * @pre		The alien was running in the given direction at the invocation of the method,
	            and not standing still.
	 * 			| alien.getRunning() == direction
	 * @effect	The alien's running status will be changed to standing still.
	 * 			| this.setRunning(Running.STOP)
	 * @post	The alien's acceleration in x direction is changed to 0.
	 * 			The y acceleration of the alien is left unchanged.
	 * 			| new.getAcceleration() == {0.0,this.getAcceleration()[1]}
	 * @post	The alien's velocity in x direction is changed to 0.
	 * 			The y velocity of the alien is set to zero.
	 * 			| new.getVelocity() == {0.0,0.0}
	 * @effect	The time the alien had been moving is reset to 0.
	 * 			| this.setTimeMoving(0.0)
	 * @effect	The index of the array of sprites for a moving alien is reset to 0.
	 * 			| this.setIndexSprites(0);
	 */
	public void endMove(Running direction){
		assert this.getRunning() == direction;
		this.setRunning(Running.STOP);
		double[] newValues = {0.0,this.getAcceleration()[1]};
		this.setAcceleration(newValues);
		double[] newValuesVel = {0.0,0.0};
		this.setVelocity(newValuesVel);
		this.setTimeMoving(0.0);
		this.setIndexSprites(0);
	}

    // JUMPING //
    
	private boolean jumping=false;
	
	/**
	 * Check if this alien is jumping or not.
	 */
	@Basic
	public boolean isJumping() {
		return jumping;
	}
	
	/**
	 * Set the jumping state of the alien to the given boolean.
	 * @param 	jumping
	 * 			The new jumping state for this alien
	 * @post	The new jumping state for this alien
	 * 			is equal to the given boolean. 
	 *          | new.isJumping() == jumping
	 */
	public void setJumping(boolean jumping) {
		this.jumping = jumping;
	}
	
	/**
	 * Make the given alien jump.
	 * @effect	The current jumping state of this alien is set to true.
	 * 			| this.setJumping(true)
	 * @post	The y acceleration is set to the static acceleration in y direction.
	 * 			The x acceleration of this alien is left unchanged.
	 * 			| new.getAcceleration() == {this.getAcceleration()[0],Mazub.getAccelerationValues()[1]}
	 * @post	The y velocity is set to the static initial velocity in y direction.
	 * 			The x velocity of this alien stays unchanged.
	 * 			| new.getVelocity() == {this.getVelocity()[0],Mazub.getInitialVelocityY()}
	 * @throws 	IllegalArgumentException
	 * 			The current y position of the alien doesn't allow the alien to jump.
	 * 			| this.getPixelBottomY() > 0
	 */
    public void startJump() throws IllegalStateException {
		if ( this.getPixelBottomY() > 0)
			throw new IllegalStateException();
		this.setJumping(true);
		double[] newValues = {this.getAcceleration()[0],Mazub.getAccelerationValues()[1]};
		this.setAcceleration(newValues);
		double[] newValuesVel = {this.getVelocity()[0],Mazub.getInitialVelocityY()};
		this.setVelocity(newValuesVel);
	}

	/**
	 * End the given alien's jump.
	 * @effect	The current jumping state of this alien is set to false.
	 * 			| this.setJumping(false)
	 * @post	The y velocity of the alien is set to 0. The velocity in x direction stays the same.
	 * 			| new.getVelocity() == {this.getVelocity()[0],0.0}	
	 * @throws 	IllegalStateException
	 * 			If the alien wasn't jumping at the time of invocation of the method,
	 * 			the alien can't end his jump, therefore the method can't be executed.
	 *          | this.isJumping == False
	 */
	public void endJump() throws IllegalStateException {
		if ( this.isJumping() == false)
			throw new IllegalStateException();
		this.setJumping(false);
		double[] newValues = {this.getAcceleration()[0],Mazub.getAccelerationValues()[1]};
		this.setAcceleration(newValues);
		double[] newValuesVel = {this.getVelocity()[0],0.0};
		this.setVelocity(newValuesVel);
	}
	
	// DUCKING //
	
	private boolean ducking=false;
	
	/**
	 * Check if this alien is ducking or not.
	 */
	@Basic
	public boolean isDucking() {
		return ducking;
	}

	/**
	 * Set the ducking state of this alien to the given boolean.
	 * 
	 * @param 	ducking
	 * 			The new ducking state for this alien.
	 * @post	The new ducking state for this alien is changed to the given flag.
	 *          | new.isDucking() == ducking
	 */
	public void setDucking(boolean ducking) {
		this.ducking = ducking;
	}

	/**
	 * Make the alien start ducking.
	 * 
	 * @effect	The ducking state of this alien is set to true.
	 * 			| this.setDucking(true)
	 * @effect	The maximum velocity for this alien is set 1.0
	 * 			| setMaximumVelocity(1.0)
	 * @post	The time since this alien's last action is reset to 0.0
	 * 			| new.getTimeLastAction == 0
	 * @throws 	IllegalStateException
	 * 			If this alien is jumping, he can't start ducking.
	 * 			| this.isJumping()
	 */
	public void startDuck() throws IllegalStateException {
		if (this.isJumping())
			throw new IllegalStateException("Alien can't duck while jumping");
		this.setDucking(true);
		setMaximumVelocity(1.0);
		setTimeLastAction(0.0);
	}

	/**
	 * Make the alien stop ducking.
	 * 
	 * @effect	The alien's current ducking state is set to false.
	 * 			| setDucking(false)
	 * @post	The alien's maximum velocity is reset to its maximum velocity before he was ducking.
	 * 			| new.getMaximumVelocity() == this.getInitialMaxVelocity()
	 * @throws 	IllegalStateException
	 * 			If the alien wasn't ducking yet, he can't stop ducking.
	 *          | ! this.isDucking()
	 */
	public void endDuck() throws IllegalStateException {
		if (! this.isDucking())
			throw new IllegalStateException("Alien isn't ducking");
		this.setDucking(false);
		setMaximumVelocity(getInitialMaxVelocity());
	}
	
// Advance time

	/**
	 * Advance the state of the given alien by the given time period.
	 * @param   dt
	 *          The time interval (in seconds) by which to advance the given
	 *          alien's time.
	 * @effect  The acceleration and velocity is set to zero when the alien hits the bottom.
	 * 			| this.alienHitsBottom()
	 * @effect  The location is updated by the given time period.
	 * 			| this.updateLocation()
	 * @effect The velocity is updated by the given time period.
	 * 			|this.updateVelocity()
	 * @post    If the alien is not running, the time last action is set to
	 * 			the current time last action incremented by dt.
	 * 			|if this.getRunning() == Running.STOP
	 * 			|	new.getTimeLastAction() = this.getTimeLastAction() + dt
	 * @effect  If the alien is running, the index is updated.
	 * 			| if this.getRunning() != Running.STOP	
	 * 			|	this.updateIndexSprites(dt)
	 */
	public void advanceTime(double dt) throws IllegalArgumentException {
		
		alienHitsBottom();
		updateLocation(dt);
		updateVelocity(dt);

        if ( this.getRunning() == Running.STOP)
			setTimeLastAction(getTimeLastAction()+dt);
		else
			updateIndexSprites(dt);
	}
	
	/**
	 * Set the acceleration and velocity to zero when the alien hits the bottom.
	 * @post The y acceleration is set to 0 and the x acceleration is left unchanged
	 * 		    | new.getAcceleration() = {this.getAcceleration()[0],0.0}
	 * @post The y velocity is set to 0 and the x velocity is left unchanged
	 * 		    | new.getVelocity() = {this.getVelocity()[0],0.0}
	 */
	public void alienHitsBottom(){
		
		if (( ! isJumping()) && ( getPixelBottomY() <= 0) && (getAcceleration()[1] <0)){
			double[] newValuesAcc = {this.getAcceleration()[0],0.0};
			this.setAcceleration(newValuesAcc);
			double[] newValuesVel = {this.getVelocity()[0],0.0};
			this.setVelocity(newValuesVel);
		}
	}
	
	/**
	 * Advance the velocity of the alien by the given time period.
	 *
	 * @param   dt
	 * 			The time interval (in seconds) by which to advance the given
	 *            alien's time.
	 * @post    The y velocity is set to the current y velocity incremented by dt times the current 
	 * 			y acceleration.
	 * 			| new.getVelocity()[1] = this.getVelocity()[1] + this.getAcceleration()[1]*dt
	 * @post    The x velocity is set to the current x velocity incremented by dt times the current
	 * 			x acceleration, if it doesn't exceed the maximum velocity
	            | if new.getVelocity()[0] <= this.getMaximumVelocity()
	 * 			|   new.getVelocity()[0] = this.getVelocity()[0] +this.getAcceleration()[0]*dt
	 * @post    The x velocity is set to the maximum velocity if the new x velocity exceeds the maximum velocity
	 * 			| if new.getVelocity()[0] > this.getMaximumVelocity()
	 * 				new.getVelocity()[0] = this.getMaximumVelocity()
	 */
	public void updateVelocity(double dt){
		double horizontalVelocity = this.getVelocity()[0] + this.getAcceleration()[0]*dt;
		if (horizontalVelocity > getMaximumVelocity())
			horizontalVelocity = getMaximumVelocity();
		double verticalVelocity = this.getVelocity()[1] + this.getAcceleration()[1]*dt;
		
		double[] newVelocity = {horizontalVelocity,verticalVelocity};
		setVelocity(newVelocity);
	}
	
	/**
	 * Update the given location of this alien by the given time period.
	 * 
	 * @param dt
	 * 		  The time interval (in seconds) by which to advance the alien's time.
	 * @post	The new x coordinate of the alien is its old x coordinate incremented (if moving to the right) or
	 * 			decremented (when moving to the left) with the new distance crossed by the alien in horizontal direction.
	 *			| if this.getRunning() == Running.Right
	 * 			| 	then new.getPixelLeftX() == this.getPixelLeftX() + distanceX
	 * 			| else
	 * 			|	then new.getPixelLeftX() == this.getPixelLeftX() - distanceX
	 * @post	The new y coordinate of the alien is its old y coordinate incremented with
	 * 			the new distance crossed by the alien in vertical direction. If the velocity of the alien is negative,
	 * 			that means the alien is moving towards to earth, and that means distanceY will be negative.
	 * 			| new.getPixelBottomY() == this.getPixelBottomY + distanceY
	 * @effect	If the alien is moving towards the earth (negative velocity and negative distanceY)
	 * 			and its next y coordinate would be below zero when updated,
	 * 			its y coordinate is set to zero.
	 * 			| if (! isValidY(this.getPixelBottomY() + (int) (100*distanceY)))
				|	setPixelBottomY(0);
	 * @throws	IllegalArgumentException
	 * 			If the new x coordinate of the alien is too big or too small and thereby not a valid pixel,
	 * 			the new coordinate will not be set to the new x coordinate.
	 * 			| (! isValidX(this.getPixelLeftX() -(int) (100*distanceX))
	 * 			|	|| (! isValidX(this.getPixelLeftX() + (int) (100*distanceX)))
	 */
	public void updateLocation(double dt){
		double distanceX = this.getVelocity()[0]*dt + 0.5*this.getAcceleration()[0]*dt*dt;
		double distanceY = this.getVelocity()[1]*dt + 0.5*this.getAcceleration()[1]*dt*dt;
		
		if (this.getRunning() == Running.LEFT)
			if (! isValidX(this.getPixelLeftX() -(int) (100*distanceX)))
				throw new IllegalArgumentException("Alien x position out of reach");
			else
				this.setPixelLeftX(this.getPixelLeftX() - (int) (100*distanceX)); 
		else
			if (! isValidX(this.getPixelLeftX() + (int) (100*distanceX)))
				throw new IllegalArgumentException("Alien x position out of reach");
			else
				this.setPixelLeftX(this.getPixelLeftX() +(int) (100*distanceX));
		
		if (! isValidY(this.getPixelBottomY() + (int) (100*distanceY)))
			this.setPixelBottomY(0);
		else
			this.setPixelBottomY(this.getPixelBottomY() + (int) (100*distanceY));
	}
	
	/**
	 * Advance the indexSprites of this alien by the given time period or advances this alien's timeMoving.
	 *
	 * @param   dt
	 * 			The time interval (in seconds) by which to advance the given
	 *           alien's time.
	 * @post    The timeMoving is set to the current time moving incremented by dt
	 * 			| new.getTimeMoving() == this.getTimeMoving() + dt
	 * @post    If timeMoving exceeds 0.075 s, the alien's indexSprites is incremented by 1
	 * 			and its timeMoving is reset to 0.
	 * 			| if (getTimeMoving() >= 0.075) 
	 *          |       if (getIndexSprites() == 10)
	 *          |           then new.getIndexSprites() == 0
	 *          |       else then new.getIndexSprites() == this.getIndexSprites() +1
	 * 			|    then new.getTimeMoving() == 0	
	 */
	public void updateIndexSprites (double dt){
		setTimeMoving(getTimeMoving()+dt);
		if (getTimeMoving() >= 0.075){
			if (getIndexSprites() == 10)
				setIndexSprites(0);
			else
				setIndexSprites(getIndexSprites()+1);
			setTimeMoving(0);
			}
	}

// Declaration of class variables and instance variables 
//              for given initial and maximum values for velocity and acceleration.

	public static double[] accelerationValues = {0.9,-10.0};
	public double initialVelocityX = 1.0;
	public static double initialVelocityY = 8.0;
	public double maximumVelocity = 3.0;
	public double initialMaxVelocity = 3.0;
	
	/**
	 * Return an array of doubles. The first one is the acceleration of a Mazub in the x direction.
	 * The second double is the acceleration in the y direction.
	 */
	@Basic @Immutable
	public static double[] getAccelerationValues()
	{
		return accelerationValues;
	}
	
	/**
	 * Return the initial velocity in the x direction of this alien.
	 */
	@Basic
	public double getInitialVelocityX()
	{
		return initialVelocityX;
	}
	
	/**
	 * Return the initial velocity in the y direction for a Mazub.
	 */
	public static double getInitialVelocityY()
	{
		return initialVelocityY;
	}
	
	/**
	 * Return the maximum velocity in the x direction of this alien.
	 */
	@Basic
	public double getMaximumVelocity()
	{
		return maximumVelocity;
	}
	
	/**
	 * Set the initialVelocity in the x direction to the new value.
	 * 
	 * @param 	initialVelocityX
	 * 			The alien's new initial velocity for the x direction.
	 * @post	If the given double is bigger than or equal to 1.0,
	 *          the initialVelocity in the x direction is set to the new value
	 *          | if InitialVelocityX >1
	 *          |   then new.getInitialVelocityX() == initialVelocityX
	 * 
	 */
	public void setInitialVelocityX(double initialVelocityX)
	{
		if (initialVelocityX > 1)
			this.initialVelocityX = initialVelocityX;
	}
	
	/**
	 * Return the original value for the maximum velocity in x direction of this alien.
	 */
	public double getInitialMaxVelocity()
	{
		return this.initialMaxVelocity;
	}
	
	/**
	 * Set the original value for the maximum velocity to the given value.
	 * 
	 * @param 	initialMaxVelocity
	 * 			The new value for the initial maximum velocity.
	 * @post	The new initial max velocity is changed to the given value.
	 * 			|new.getInitialMaxVelocity() == initialMaxVelocity.
	 */
	public void setInitialMaxVelocity(double initialMaxVelocity)
	{
		this.initialMaxVelocity = initialMaxVelocity;
	}
	
	/**
	 * Set the value for the maximum velocity to the given value.
	 * 
	 * @param 	MaxVelocity
	 * 			The new value for the maximum velocity.
	 * @post    If the alien is not ducking and the given value exceeds the
	 *          initial x velocity, the new max velocity and the new initial max 
	 *          velocity are changed to the given value.
	 * 			|if !isDucking()
	 *          |   then if maxVelocity >= this.getInitialVelocityX()
	 *          |       then new.getMaximumVelocity() == maxVelocity
	 *          |            new.getInitialMaxVelocity() == maxVelocity
	 * @post    If the alien is ducking, the new max velocity is changed to the 
	 *          given value.
	 *          |if isDucking()
	 *          |   then new.getMaximumVelocity() == maxVelocity
	 */
	public void setMaximumVelocity(double maxVelocity)
    {
		if (this.isDucking())
			this.maximumVelocity = maxVelocity;
		else 
			if(maxVelocity >= this.getInitialVelocityX()){
				this.maximumVelocity = maxVelocity;
				setInitialMaxVelocity(maxVelocity);
			}
	}
	
// Sprites

	private Sprite[] sprites;
	
	/**
	* Return the array of sprites of this alien.
	*/
	@Basic
	public Sprite[] getSprites(){
		return this.sprites;
	}
	
	/**
	 * Returns the image corresponding to the current action of the alien.
	 * 
	 * @pre		The alien's sprites has a length equal to 30.
	 * 			| this.getSprites().length == 30
	 * @pre		The alien is existing.
	 * 			| this != null
	 * @pre		The alien is running in a valid direction
	 * 			| isValidRunning(this.getRunning())
	 * @return	If the alien is not moving and not ducking and he hasn't been running in the last second,
	 * 			the method returns this.getSprites()[0].
	 * @return	If the alien is not moving and not ducking but he has been running to the right in the last second,
	 * 			the method returns this.getSprites()[2].
	 * @return 	If the alien is not moving and not ducking but he has been running to the left in the last second,
	 * 			the method returns this.getSprites()[3].
	 * @return	If the alien is not moving and hasn't been running in the last second, but he is ducking,
	 * 			the method returns this.getSprites()[1].
	 * @return	If the alien is not moving but he is ducking, and he has been running to the right in the last second,
	 * 			the method returns this.getSprites()[6].
	 * @return	If the alien is not moving but he is ducking, and he has been running to the left in the last second,
	 * 			the method returns this.getSprites()[7].
	 * @return	If the alien is running to the right and is not ducking but is jumping, 
	 * 			the method returns this.getSprites()[4].
	 * @return 	If the alien is running to the left and is not ducking but is jumping, 
	 * 			the method returns this.getSprites()[5].
	 * @return	If the alien is running to the right and is not ducking nor jumping, 
	 * 			the method returns this.getSprites()[8+getIndexSprites()].
	 * @return 	If the alien is running to the left and is not ducking nor jumping, 
	 * 			the method returns this.getSprites()[19+getIndexSprites()].
	 * @return	If the alien is running to the right and is ducking but not jumping,
	 * 			the method returns this.getSprites()[6].
	 * @return If the alien is running to the left and is ducking but not jumping,
	 * 			the method returns this.getSprites()[7].
	 */
	public Sprite getCurrentSprite() {
		assert this.getSprites().length == 30;
		assert this != null;
		assert isValidRunning(this.getRunning());
		if (getRunning() == Running.STOP){
			if (! isDucking()){
				if  ((getLastDirection() == Running.STOP) || (getTimeLastAction() > 1.0))
					return getSprites()[0];
				else{
					if (getLastDirection() == Running.RIGHT)
						return getSprites()[2];
					else
						return getSprites()[3];
				}
			}
			else {
				if ((getLastDirection() == Running.STOP) || (getTimeLastAction() > 1.0))
					return getSprites()[1];
				else{
					if (getLastDirection() == Running.RIGHT)
						return getSprites()[6];
					else
						return getSprites()[7];
				}
			}
		}
		else if (getRunning() == Running.RIGHT){
			if ( (! isDucking()) && ( isJumping()) )
				return getSprites()[4];
			else if ( (! isDucking()) && ( ! isJumping()) )
				return getSprites()[8+getIndexSprites()];
			else
				return getSprites()[6];
		}
		else{ // getRunning() == Running.LEFT
			if ( (! isDucking()) && ( isJumping()) )
				return getSprites()[5];
			else if ( (! isDucking()) && ( ! isJumping()) )
				return getSprites()[19+getIndexSprites()];
			else
				return getSprites()[7];
			}
		}
	
// Declaration of multiple variables necessary for a correct return of getCurrentSprite() //

	private double timeLastAction = 0.0;
	private double timeMoving = 0.0;
	private int indexSprites = 0;

    /**
	 * Return the index of the current sprite of the running alien.
	 */
	public int getIndexSprites() {
		return indexSprites;
	}
	
	/**
	* Set the index of the current sprite of the running alien to the given value.
	*
	* @param 	indexSprite
	* 			The new value for the index of the current sprite.
	* @post	    The new index is changed to the given value.
    *			| new.getIndexSprites() == indexSprites
	*/
	public void setIndexSprites(int indexSprites) {
		this.indexSprites = indexSprites;
	}
	
	/**
	 * Return the time the alien has been moving to the left or the right.
	 */
	public double getTimeMoving() {
		return timeMoving;
	}
	
	/**
	* Set the time the alien has been moving to the left or the right to the given value.
	*
	* @param 	timeMoving
	* 			The new value for the time moving.
	* @post	    The new timeMoving is changed to the given value.
    *			| new.getTimeMoving() == timeMoving
	*/
	public void setTimeMoving(double timeMoving) {
		this.timeMoving = timeMoving;
	}
	
	/**
	 * Return the time passed since the last time the alien completed an action.
	 */
	public double getTimeLastAction() {
		return timeLastAction;
	}
	
	/**
	* Set the time passed since the last time the alien completed an action.
	* @param 	timeLastAction
	* 			The new value for the time since the last action.
	* @post	    The new time since last action is changed to the given value.
    *			| new.getTimeLastAction() == timeLastAction
	*/

	public void setTimeLastAction(double timeLastAction) {
		this.timeLastAction = timeLastAction;
	}
	
	private Running lastDirection=Running.STOP;

	/**
	 * Return the last direction the alien was moving in.
	 */
	public Running getLastDirection() {
		return lastDirection;
	}
	
    /**
	* Set the last direction the alien was moving in.
	* @param 	lastDirection
	* 			The new running for the last direction.
	* @post	    The new direction is changed to the given running.
    *			| new.getLastDirection() == lastDirection
	*/

	public void setLastDirection(Running lastDirection) {
		this.lastDirection = lastDirection;
	}
	
	
}