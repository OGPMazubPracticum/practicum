package jumpingalien.part2.facade;

import java.util.Collection;

import jumpingalien.util.ModelException;
import jumpingalien.model.Coordinate;
import jumpingalien.model.Mazub;
import jumpingalien.model.Plant;
import jumpingalien.model.Running;
import jumpingalien.model.School;
import jumpingalien.model.Shark;
import jumpingalien.model.Slime;
import jumpingalien.model.World;
import jumpingalien.part1.facade.IFacade;
import jumpingalien.util.Sprite;
import jumpingalien.model.IllegalPositionException;

public class Facade implements IFacadePart2,IFacade {
	
	public int getNbHitPoints(Mazub alien){
		return alien.getHitPoints();
	}
	
	public World createWorld(int tileSize, int nbTilesX, int nbTilesY,
			int visibleWindowWidth, int visibleWindowHeight, int targetTileX,
			int targetTileY){
		return new World(tileSize,nbTilesX,nbTilesY,visibleWindowHeight,visibleWindowWidth,
				targetTileX,targetTileY);
	}
	
	public int[] getWorldSizeInPixels(World world){
		return world.getWorldSizeInPixels();
	}
	
	public int getTileLength(World world){
		return world.getTileSize();
	}
	
	public void startGame(World world){
	}
	
	public boolean isGameOver(World world){
		return world.isGameOver();
	}
	
	public boolean didPlayerWin(World world){
		return world.didPlayerWin();
	}

	public void advanceTime(World world, double dt){
		try{
			world.advanceTime(dt);
		} catch (IllegalArgumentException exc) {
		}
	}
	
	public int[] getVisibleWindow(World world){
		return world.getVisibleWindow();
	}

	
	public int[] getBottomLeftPixelOfTile(World world, int tileX, int tileY){
		Coordinate coordinates = world.getBottomLeftPixelOfTile(tileX, tileY);
		int[] result = {coordinates.getPixelLeftX(),coordinates.getPixelBottomY()};
		return result;
	}
	
	public int[][] getTilePositionsIn(World world, int pixelLeft, int pixelBottom,
			int pixelRight, int pixelTop){
		return world.getTilePositionsIn(pixelLeft, pixelBottom, pixelRight, pixelTop);
	}
	
	public int getGeologicalFeature(World world, int pixelX, int pixelY){
		try{
			return world.getGeologicalFeature(pixelX, pixelY);
		} catch (IllegalPositionException exc) {
			throw new ModelException("One or both of the given arguments is out of reach");
		}
	}
	
	public 	void setGeologicalFeature(World world, int tileX, int tileY, int tileType){
		world.setGeologicalFeature(tileX, tileY, tileType);
	}
	
	public void setMazub(World world, Mazub alien){
		try {
		world.addMazub(alien);
		} catch (NullPointerException exc){
			throw new ModelException("There already is a player character Mazub in this world");
		}
	}
	
	public boolean isImmune(Mazub alien){
		return alien.isImmune();
	}

	public Plant createPlant(int x, int y, Sprite[] sprites){
		return new Plant(x,y,sprites);
	}
	
	public void addPlant(World world, Plant plant){
		world.addPlant(plant);
	}
	
	public Collection<Plant> getPlants(World world){
		try {
			return world.getAllPlants();
		} catch (NoSuchMethodException exc){
			throw new ModelException("Method not found");
		}
	}
	
	public int[] getLocation(Plant plant){
		int[] location = {plant.getLocation().getPixelLeftX(),plant.getLocation().getPixelBottomY()};
		return location;
	}

	public Sprite getCurrentSprite(Plant plant){
		return plant.getCurrentSprite();
	}
	
	public Shark createShark(int x, int y, Sprite[] sprites){
		return new Shark(x,y,sprites);
	}
	
	public void addShark(World world, Shark shark){
		world.addShark(shark);
	}
	
	public Collection<Shark> getSharks(World world){
		try {
			return world.getAllSharks();
		} catch (NoSuchMethodException exc){
			throw new ModelException("Method not found");
		}
	}
	
	public int[] getLocation(Shark shark){
		int[] location = {shark.getLocation().getPixelLeftX(),shark.getLocation().getPixelBottomY()};
		return location;
	}
	
	public Sprite getCurrentSprite(Shark shark){
		return shark.getCurrentSprite();
	}
	
	public School createSchool(){
		return new School();
	}
	
	public Slime createSlime(int x, int y, Sprite[] sprites, School school){
		try {
		return new Slime(x,y,sprites,school);
		} catch (IllegalArgumentException exc){
			throw new ModelException("School doesn't exist yet.");
		}
	}
	
	public void addSlime(World world, Slime slime){
		world.addSlime(slime);
	}
	
	public Collection<Slime> getSlimes(World world){
		try {
			return world.getAllSlimes();
		} catch (NoSuchMethodException exc){
			throw new ModelException("Method not found");
		}
	}
	
	public int[] getLocation(Slime slime){
		int[] location = {slime.getLocation().getPixelLeftX(),slime.getLocation().getPixelBottomY()};
		return location;
	}

	public Sprite getCurrentSprite(Slime slime){
		return slime.getCurrentSprite();
	}
	
	public School getSchool(Slime slime){
		return slime.getSchool();
	}
	
	
	///////////////////////////////////////////////////////////////
	
	public Mazub createMazub(int pixelLeftX, int pixelBottomY, Sprite[] sprites){
		try{
			return new Mazub(pixelLeftX,pixelBottomY,sprites);
		} catch(IllegalPositionException exc) {
			throw new ModelException("X or Y pixel out of reach");
		}
	}

	public int[] getLocation(Mazub alien){
		int[] alienLocation = {alien.getLocation().getPixelLeftX(),alien.getLocation().getPixelBottomY()};
		return alienLocation;
	}
	
	public Sprite getCurrentSprite(Mazub alien){
		return alien.getCurrentSprite();
	}
	
	
	public int[] getSize(Mazub alien){
		try {
		return alien.getSize();
		} catch(NullPointerException exc) {
			throw new ModelException("Null Pointer Reference");
		}
	}
	
	public double[] getVelocity(Mazub alien){
		return alien.getVelocity();
	}
	
	public double[] getAcceleration(Mazub alien){
		return alien.getAcceleration();
	}

	public void startJump(Mazub alien){
		try {
			alien.startJump();
		}
		catch (IllegalStateException exc){
			//Do nothing//
		}
	}

	public void endJump(Mazub alien){
		try {
			alien.endJump();
		}
		catch (IllegalStateException exc){
			// Do nothing//
		}
	}
	
	public void startMoveLeft(Mazub alien){
		alien.startMove(Running.LEFT);
	}

	public void endMoveLeft(Mazub alien){
		alien.endMove(Running.LEFT);
	}

	public void startMoveRight(Mazub alien){
		alien.startMove(Running.RIGHT);
	}
	
	public void endMoveRight(Mazub alien){
		alien.endMove(Running.RIGHT);
	}

	public void startDuck(Mazub alien){
	    try{
	        alien.startDuck();
	   } catch (IllegalStateException exc){
	   }
	}
	
	public void endDuck(Mazub alien){
		try {
			alien.endDuck();
		} catch (IllegalStateException exc){
			// Do nothing
		}
	}
	
	@Override
	@Deprecated
	public void advanceTime(Mazub alien, double dt){
		try {
			alien.advanceTime(dt);
		}
		catch (IllegalArgumentException exc){
		}
	}
}