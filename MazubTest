import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static jumpingalien.tests.util.TestUtils.spriteArrayForSize;
import static jumpingalien.tests.util.TestUtils.intArray;
import static jumpingalien.tests.util.TestUtils.doubleArray;
//import jumpingalien.model.Coordinate;
import jumpingalien.model.IllegalPositionException;
import jumpingalien.model.Mazub;
import jumpingalien.model.Tile;
import jumpingalien.model.TileType;
import jumpingalien.model.World;
import jumpingalien.model.Running;
import jumpingalien.util.Sprite;
import jumpingalien.util.Util;

public class MazubTest {
	
	public static Sprite[] sprites = spriteArrayForSize(2,2);
	public static Sprite[] sprites30 = spriteArrayForSize(30,30);
	public static Sprite[] sprites35 = spriteArrayForSize(35,35);
	
	private Mazub movingAlien,movingAlienCorner,jumpingAlien,jumpingAlien2,winningAlien,collidingAlien,bigAlien;
	private World newWorld,bigWorld;
	
	@Before
	public void setUp(){
		newWorld = new World(50, 10, 8, 1, 1, 3, 1);
		for(int i=0;i<10;i++){
			newWorld.getListTiles().get(0).get(i).setFeature(TileType.SOLID_GROUND);
		}
		bigWorld = new World(30,10,8,100,100,9,4);
		for(int col=0;col<10;col++){
			bigWorld.getListTiles().get(0).get(col).setFeature(TileType.SOLID_GROUND);
		}
		for(int row=0;row<8;row++){
			bigWorld.getListTiles().get(row).get(0).setFeature(TileType.SOLID_GROUND);
		}
		for(int row=0;row<5;row++){
			bigWorld.getListTiles().get(row).get(5).setFeature(TileType.SOLID_GROUND);
		}
		bigWorld.getListTiles().get(2).get(4).setFeature(TileType.SOLID_GROUND);
		for(int row=0;row<5;row++){
			bigWorld.getListTiles().get(row).get(9).setFeature(TileType.SOLID_GROUND);
		}
		bigWorld.getListTiles().get(4).get(8).setFeature(TileType.SOLID_GROUND);
		for(int row=1;row<4;row++){
			for(int col=6;col<9;col++){
				bigWorld.getListTiles().get(row).get(col).setFeature(TileType.WATER);
			}
		}
		for(int col=1;col<10;col++){
			bigWorld.getListTiles().get(7).get(col).setFeature(TileType.SOLID_GROUND);
		}
		movingAlien = new Mazub(30,49,sprites);
		movingAlienCorner = new Mazub(0,49,sprites);
		jumpingAlien = new Mazub(20,49,sprites);
		jumpingAlien2 = new Mazub(50,55,sprites);
		winningAlien = new Mazub(150,49,sprites);
		collidingAlien = new Mazub(120,89,sprites30);
		bigAlien = new Mazub(60,29,sprites35);
		alien30 = new Mazub(30,29,sprites30);
	}
	
	// Tests for setPixelLeftX and setPixelBottomY //
	
	@Test
	public void setPixelLeftX_LegalValue(){
		newWorld.addMazub(movingAlien);
		movingAlien.setLocation(10,movingAlien.getLocation().getPixelBottomY());
		assertEquals(movingAlien.getLocation().getPixelLeftX(),10);
	}
	@Test(expected=IllegalPositionException.class)
	public void setPixelLeftX_IllegalValueBelowZero(){
		newWorld.addMazub(movingAlien);
		movingAlien.setLocation(-100,movingAlien.getLocation().getPixelBottomY());
	}
	@Test(expected=IllegalPositionException.class)
	public void setPixelLeftX_IllegalValueTooHigh(){
		newWorld.addMazub(movingAlien);
		movingAlien.setLocation(newWorld.getWorldSizeInPixels()[0],movingAlien.getLocation().getPixelBottomY());
	}
	@Test
	public void setPixelLeftY_LegalValue(){
		newWorld.addMazub(movingAlien);
		movingAlien.getLocation().setPixelBottomY(10);
		assertEquals(movingAlien.getLocation().getPixelBottomY(),10);
	}
	@Test(expected=IllegalPositionException.class)
	public void setPixelLeftY_IllegalValueBelowZero(){
		newWorld.addMazub(movingAlien);
		movingAlien.setLocation(movingAlien.getLocation().getPixelLeftX(),-100);
	}
	@Test(expected=IllegalPositionException.class)
	public void setPixelLeftY_IllegalValueTooHigh(){
		newWorld.addMazub(movingAlien);
		movingAlien.setLocation(movingAlien.getLocation().getPixelLeftX(),newWorld.getWorldSizeInPixels()[1]);
	}
	
	// Tests for startMove and endMove and advanceTime //
	
	@Test
	public void startMoveRight_MaximumVelocityReached(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(2.3);
		assert 03.00 == movingAlien.getVelocity()[0];
		}
	@Test(expected=IllegalPositionException.class)
	public void advanceTime_RunningOutOfReach(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.LEFT);
		movingAlien.advanceTime(4.0);
		}
	@Test(expected=IllegalPositionException.class)
	public void advanceTime_IllegalLocation(){
		newWorld.addMazub(movingAlienCorner);
		movingAlienCorner.startMove(Running.LEFT);
		movingAlienCorner.advanceTime(0.1);
	}	
	@Test
	public void startMoveLeftCorrect(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.LEFT);
		movingAlien.advanceTime(0.2);
		// new x_position
		// 0 m/s + 1 m/s * 0.2 s + 0.5*0.9 m/s²*(0.2 s)² = 0.218 m = 21.8 cm => 21 pixels to the left
		// startPosition = 30 => endPosition = 9
		int[] newLocation = {movingAlien.getLocation().getPixelLeftX(),
								movingAlien.getLocation().getPixelBottomY()};
		assertArrayEquals(intArray(9,49),newLocation);
	}
	
/*	@Test(expected=AssertionError.class)
	public void startMove_BothDirections() throws Error{
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.2);
		movingAlien.startMove(Running.LEFT);
	}*/

	// Tests for startJump and endJump and alienHitsBottom //
	
	//@Test(expected=IllegalStateException.class)
	public void startJump_IllegalCase()
		throws Exception{
		newWorld.addMazub(jumpingAlien2);
			jumpingAlien2.startJump();
	}
	@Test
	public void startJumpCorrect(){
		newWorld.addMazub(jumpingAlien);
		jumpingAlien.startJump();
		jumpingAlien.advanceTime(0.32);
		// new y position => 0.49 + 8 m/s * 0.32 s + 0.5*(-10 m/s²)*(0.32 s)^2 = 2.538 m = 253.8 cm => new Y location = 253 cm
		// startY = 49 => endY = 273
		// startX = 20 => endX = 20
//		jumpingAlien.endJump();
//		jumpingAlien.advanceTime(1.0);
		int[] newLocation = {jumpingAlien.getLocation().getPixelLeftX(),
								jumpingAlien.getLocation().getPixelBottomY()};
		assertArrayEquals(intArray(20,253),newLocation);
	}
	
	/*@Test
	public void alienHitsBottomCorrect(){
		newWorld.addMazub(jumpingAlien);
		jumpingAlien.startJump();
		jumpingAlien.advanceTime(0.2);
		// The alien reaches a height of 1.4 m = 140 cm -> 140 pixels
		jumpingAlien.endJump();
		jumpingAlien.advanceTime(0.6);
		// new y position => 0 + 0 m/s *0.6 s +  0.5*(-10 m/s²)*(0.6 s)² = -1.80 m = -180 cm -> 180 pixels lower than 140
		// But (50,-40) is an illegal location, so (50,0) has to be the new location.
		int[] newLocation = intArray(jumpingAlien.getLocation().getPixelLeftX(),
										jumpingAlien.getLocation().getPixelBottomY());
		assertArrayEquals(intArray(50,0),newLocation);
	}*/
	
	// Tests for startDuck and endDuck //
	
	@Test
	public void startDuck_MaximumVelocityChanged(){
		newWorld.addMazub(movingAlien);
		movingAlien.startDuck();
		assertEquals(1.0,movingAlien.getMaximumVelocity(),Util.DEFAULT_EPSILON);
	}
	@Test(expected=IllegalStateException.class)
	public void startDuck_IllegalCase() throws IllegalStateException{
		newWorld.addMazub(jumpingAlien2);
		jumpingAlien2.setJumping(true);
		jumpingAlien2.startDuck();
	}
	@Test(expected=IllegalStateException.class)
	public void endDuck_IllegalCase() throws IllegalStateException{
		newWorld.addMazub(jumpingAlien);
		jumpingAlien.endDuck();
	}
	
	// Tests for advanceTime & updateVelocity //
	
	@Test
	public void advanceTime_Ducking(){
		newWorld.addMazub(movingAlien);
		movingAlien.startDuck();
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.5);
		assertArrayEquals(doubleArray(1.0,0.0),movingAlien.getVelocity(),Util.DEFAULT_EPSILON);
	}
	@Test
	public void advanceTimeRunning(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.2);
		// vx = 1 + 0.9*0.2 = 1.18
		assertArrayEquals(doubleArray(1.18,0.0),movingAlien.getVelocity(),Util.DEFAULT_EPSILON);
	}
	@Test
	public void advanceTimeRunningAndJumping(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		movingAlien.startJump();
		movingAlien.advanceTime(0.2);
		// vx = 1 + 0.9*0.2 = 1.18
		// vy = 8 + (-10)*0.2 = 8-2=6
		assertArrayEquals(doubleArray(1.18,6.0),movingAlien.getVelocity(),Util.DEFAULT_EPSILON);
	}
	
	// Tests for getCurrentSprite & advanceTime & updateIndexSprite //
	
	@Test
	public void endMove_StandingStillForLongerThanOneSecond(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.5);
		movingAlien.endMove(Running.RIGHT);
		movingAlien.advanceTime(1.05);
		assertEquals(sprites[0],movingAlien.getCurrentSprite());
	}
	@Test
	public void endMoveRight_StandingStillForLessThanOneSecond(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.5);
		movingAlien.endMove(Running.RIGHT);
		movingAlien.advanceTime(0.2);
		assertEquals(sprites[2],movingAlien.getCurrentSprite());
	}
	@Test
	public void getCurrentSprite_index1(){
		//The alien hasn't moved in the last second and is ducking.
		newWorld.addMazub(movingAlien);
		movingAlien.startDuck();
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.5);
		movingAlien.endMove(Running.RIGHT);
		movingAlien.advanceTime(1.1);
		assertEquals(sprites[1],movingAlien.getCurrentSprite());
	}
	@Test
	public void getCurrentSprite_index4(){
		//The alien is moving to the right and jumping but not ducking. //
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.1);
		movingAlien.startJump();
		movingAlien.advanceTime(0.2);
		assertEquals(sprites[4],movingAlien.getCurrentSprite());
	}
	@Test
	public void getCurrentSprite_index6(){
		//The alien is ducking, not jumping and was moving to the right within 1 second.//
		newWorld.addMazub(movingAlien);
		movingAlien.startDuck();
		movingAlien.startMove(Running.RIGHT);
		movingAlien.advanceTime(0.5);
		movingAlien.endMove(Running.RIGHT);
		movingAlien.advanceTime(0.2);
		assertEquals(sprites[6],movingAlien.getCurrentSprite());
	}
	@Test
	public void getCurrentSprite_AlienIsMoving(){
		newWorld.addMazub(movingAlien);
		movingAlien.startMove(Running.RIGHT);
		for (int i = 0; i < 5; i++) {
			movingAlien.advanceTime(0.075);
		}
		assertEquals(sprites[8+5],movingAlien.getCurrentSprite());
	}
	
	/// HitPoints & alien dies & alien wins ///
	
	@Test
	public void setHitPoints_NormalValue(){
		winningAlien.setHitPoints(200);
		assertEquals(200,winningAlien.getHitPoints());
	}
	@Test
	public void setHitPoints_MaximumReached(){
		winningAlien.setHitPoints(550);
		assertEquals(500,winningAlien.getHitPoints());
	}
	@Test
	public void setHitPoints_NegativeValue(){
		winningAlien.setHitPoints(-10);
		assertEquals(0,winningAlien.getHitPoints());
	}
	@Test
	public void alienIsDead_FalseCase(){
		//winningAlien.getHitPoints()==100
		assertEquals(false,winningAlien.alienIsDead());
	}
	@Test
	public void alienIsDead_TrueCase(){
		winningAlien.setHitPoints(0);
		assertEquals(true,winningAlien.alienIsDead());
	}
	@Test
	public void hasReachedTargetTile_TrueCase(){
		//Coordinates of winningAlien are (150,49)
		//The tileSize of newWorld is 50;
		//TargetTile newWorld = (3,1)
		//While winningAlien's bottom perimeter is located in
		// the tile with coordinates (3,0), the biggest part
		// of winningAlien is located in tile (3,1)
		newWorld.addMazub(winningAlien);
		Tile currentTile = newWorld.getTile(150,50);
		Tile targetTile = newWorld.getListTiles().get(1).get(3);
		assertEquals(targetTile,currentTile);
		assertEquals(true,winningAlien.hasReachedTargetTile());
	}
	@Test
	public void hasReachedTargetTile_FalseCase(){
		newWorld.addMazub(winningAlien);
		winningAlien.setLocation(40, 19);
		assertEquals(false,winningAlien.hasReachedTargetTile());
	}
	
	///	Impassable terrain - jumping & moving & falling & ducking ////
	@Test
	public void isGround(){
		boolean result =  bigWorld.getListTiles().get(2).get(4).getFeature().getTileType()==TileType.SOLID_GROUND;
		assertEquals(true,result);
		}
	@Test
	public void cannotMoveRight(){
		bigWorld.addMazub(collidingAlien);
		collidingAlien.startMove(Running.RIGHT);
		// startPosition = (30,29)
		bigWorld.advanceTime(0.7);
		/// Normally the new location of the alien would be 30 cm + 100*(1 /s*0.7 s + 0.5*0.9 /s²*s²) cm = 122 cm
		// But the height (and the width as well) of the alien is 35 cm and at Tile(4,2)(=> bottomLeftPixel (120,60) there is solid ground.
		// Since every tile has a size of 30 cm, this means that if the alien would be
		// located at pixel(122,29), the upper perimeter of the alien would be at height (29+35-1)=(63);
		// This would mean the alien would be crossing solid ground. So the alien stops moving before his upper perimeter
		// collides with tile (4,2).
		// This happens if his upper pixel is located at (120,63) and therefore his bottomLeftPixel must stay at (120-35,63-35+1)
		// which is equal to (85,29).
		assertEquals(85,collidingAlien.getLocation().getPixelLeftX());
	}
	@Test
	public void alienHitsSolidGroundWhileJumping(){
		bigWorld.addMazub(alien30);
		alien30.setLocation(120,89);
		alien30.startJump();
		bigWorld.advanceTime(0.15);
		/* New Y Position = 89 + (0.15*8.0 + 0.15²*0.5*-10.0)*100 = 89 +108.75 = 89 +108 = 197
		 * But the alien has a height of 30 pixels which means the alien's top perimeter would be located
		 * at a height of 197 + 30 = 227 cm. The bottom row of pixels however in the world
		 * (which has tileSize = 30 and nbTilesY = 8) has the geological feature of solid ground.
		 * This means that between 210 and 239 cm height there is solid ground.
		 * So the alien will stop jumping when his bottom left pixel is at a height of  180,
		 * so that its top row of pixels would be located at a height of 180+29 = 209 cm, just below the solid ground.
		 */
		assertEquals(180,alien30.getLocation().getPixelBottomY());
	}
	@Test
	public void alienHitsSolidGroundAfterJump(){
		bigWorld.addMazub(collidingAlien);
		collidingAlien.setLocation(120,89);
		collidingAlien.startJump();
		bigWorld.advanceTime(0.1);
		assertEquals(164,collidingAlien.getLocation().getPixelBottomY());
		assertEquals(120,collidingAlien.getLocation().getPixelLeftX());
		collidingAlien.endJump();
		bigWorld.advanceTime(0.4);
		int[] newLocation = {collidingAlien.getLocation().getPixelLeftX(),collidingAlien.getLocation().getPixelBottomY()};
		assertArrayEquals(intArray(120,89),newLocation);
	}
	@Test
	public void isAir(){
		boolean result =  (bigWorld.getListTiles().get(2).get(3).getFeature().getTileType()==TileType.AIR);
		assertEquals(true,result);
	}
	@Test
	public void AlienFallsOffSolidGround(){
		bigWorld.addMazub(alien30);
		alien30.setLocation(120,89);
		assertEquals(149,alien30.getLocation().getPixelLeftX()+alien30.getSize()[0]-1);
		alien30.startMove(Running.LEFT);
		bigWorld.advanceTime(0.3);
		alien30.endMove(Running.LEFT);
		Tile tile =  bigWorld.getTile(alien30.getLocation().getPixelLeftX(),alien30.getLocation().getPixelBottomY());
		TileType tileType = tile.getFeature().getTileType();
		bigWorld.advanceTime(0.4);
		assertEquals(TileType.AIR,tileType);
		assertEquals(86,alien30.getLocation().getPixelLeftX());
		assertEquals(29,alien30.getLocation().getPixelBottomY());
	}
	@Test
	public void HeadHitsSolidGround(){
		bigWorld.addMazub(collidingAlien);
		collidingAlien.setLocation(85,29);
		bigAlien.startMove(Running.RIGHT);
		bigWorld.advanceTime(0.1);
		assertEquals(85,collidingAlien.getLocation().getPixelLeftX());
		assertEquals(29,collidingAlien.getLocation().getPixelBottomY());
	}
	@Test
	public void alienFallsInWater_TooShortTimePeriod(){
		bigWorld.addMazub(collidingAlien);
		collidingAlien.setLocation(180,90);
		int tileTypeSymbol = bigWorld.getGeologicalFeature(180,90);
		assertEquals(2,tileTypeSymbol);
		bigWorld.advanceTime(0.1);
		assertEquals(100,collidingAlien.getHitPoints());
	}
	@Test
	public void alienFallsInWater_LosesHitPoints(){
		bigWorld.addMazub(collidingAlien);
		collidingAlien.setLocation(180,90);
		bigWorld.advanceTime(0.5);
		assertEquals(96,collidingAlien.getHitPoints());
	}
}